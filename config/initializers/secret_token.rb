# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Mock::Application.config.secret_key_base = 'd41a705ba19ec0fed3595003daf3c7c9166987c9816e84166072f97e4f3ac4e9bc5e013072f337a641762a0d42185af998abc9072ede2a7eab54b05e1b8a400f'
