module ApplicationHelper

  def endpoint_form(endpoints)
    bootstrap_form_tag :url => "", :html => {:class => "endpoint-form"} do |f|
      f.collection_select("endpoint", @endpoints, :url, :name)
    end
  end

  def api_keys(accounts)


    data = Hash[accounts.map do |account|
      [account.account_id, account.api_key]
    end]

    javascript_tag "var apiKeys = #{data.to_json};"
  end




end

module ActionView
  module Helpers
    module Tags
      class Base
        alias_method :tag_name_old, :tag_name

        def tag_name(multiple = false)
          tag_name_old(multiple).gsub(/^\[([^]]+)\]/, '\1')
        end
      end
    end
  end
end

