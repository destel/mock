module FormHelper
  class StandardFormBuilder < ActionView::Helpers::FormBuilder
    alias_method :simple_text_field, :text_field
    alias_method :simple_fields_for, :fields_for
    alias_method :simple_check_box, :check_box

    def field_wrapper(field, options={})
      custom_wrapper_class = "form-group"
      custom_wrapper_class += @object.errors[field].empty? ? '' : ' has-error'
      custom_wrapper_class += " #{options[:wrapper_class]}"


      @template.content_tag(:div, :class => custom_wrapper_class) do
        if options[:label] != false
          label(field, options[:label], :class => "col-sm-3 control-label")
        else
          " ".html_safe
        end +
        yield +
        if !@object.errors[field].empty?
          @template.content_tag(:p, :class => "col-sm-4 help-block field-errors") do
            @object.errors.full_messages_for(field).map do |msg|
              @template.content_tag :span, msg
            end.join.html_safe
          end
        end
      end
    end

    def hint(text)
      if text.nil? || text.empty?
        ""
      else
        @template.content_tag(:p, text, :class => 'help-block')
      end
    end

    def first_col_offset(options)
      options[:label] == false ? "col-sm-offset-3" : ""
    end

    def field(field, options={})
      field_wrapper(field, options) do
        @template.content_tag(:div, :class => "col-sm-5 #{first_col_offset(options)}") do
          yield + hint(options[:hint])
        end
      end
    end


    def self.create_tagged_field(method_name)
      define_method(method_name) do |field, *args|
        options = args.extract_options!
        options[:class] = "form-control #{options[:class]}"
        options[:wrapper_class] = "standard-#{method_name.to_s.dasherize} #{options[:wrapper_class]}"


        field(field, options) do
          super(field, *(args + [options.merge(wrapper_class: nil)]))
        end

      end
    end

    [:text_field, :password_field, :collection_select, :text_area, :grouped_collection_select].each do |name|
      create_tagged_field(name)
    end

    def datetime_text_fields(field, *args)
      options = args.extract_options!
      options[:class] = "form-control #{options[:class]}"
      options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      date_options = options.clone
      date_options[:class] = "date-subfield #{date_options[:class]}"
      date_options[:wrapper_class] = nil

      time_options = options.clone
      time_options[:class] = "time-subfield #{time_options[:class]}"
      time_options[:wrapper_class] = nil


      field_wrapper(field, options) do
        @template.content_tag(:div, :class => "col-sm-3 #{first_col_offset(options)}") do
          simple_text_field("#{field}_date", date_options)
        end +
        @template.content_tag(:div, :class => "col-sm-2") do
          simple_text_field("#{field}_time", time_options)
        end
      end
    end

    def select(field, choices, options = {}, html_options = {})
      html_options = options.clone
      html_options[:class] = "form-control #{options[:class]}"
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, choices, options, html_options.merge(wrapper_class: nil))
      end
    end


    def collection_select(field, collection, value_method, text_method, options = {}, html_options = {})
      html_options = options.clone
      html_options[:class] = "form-control #{options[:class]}"
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, collection, value_method, text_method, options, html_options.merge(wrapper_class: nil))
      end
    end

    def grouped_collection_select(field, collection, group_method, group_label_method, option_key_method, option_value_method, options = {}, html_options = {})
      html_options = options.clone
      html_options[:class] = "form-control #{options[:class]}"
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, collection, group_method, group_label_method, option_key_method, option_value_method, options, html_options.merge(wrapper_class: nil))
      end
    end

    def time_zone_select(field, priority_zones = nil, options = {}, html_options = {})
      html_options = options.clone
      html_options[:class] = "form-control #{options[:class]}"
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, priority_zones, options, html_options.merge(wrapper_class: nil))
      end
    end

    def check_box(field, options = {}, checked_value = "1", unchecked_value = "0")
      custom_label = options[:label] || @object.class.human_attribute_name(field)

      @template.content_tag :div, :class => "checkbox" do
        @template.content_tag :label do
          super(field, options, checked_value, unchecked_value) + " #{custom_label}"
        end
      end
    end

    def check_box_field(field, options = {}, checked_value = "1", unchecked_value = "0")
      field(field, options.merge(label: false)) do
        check_box(field, options, checked_value, unchecked_value)
      end
    end

    def collection_check_boxes(field, collection, value_method, text_method, options = {}, html_options = {}, &block)
      html_options = options.clone
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, collection, value_method, text_method, options, html_options.merge(wrapper_class: nil)) do |b|
          @template.content_tag :div, :class => "checkbox" do
            @template.content_tag :label do
              b.check_box + b.text
            end
          end
        end
      end
    end

    def submit(label, options={})
      super(label, options.merge(class: 'btn btn-primary'))
    end

  end

  def standard_form_for(name, *args, &block)
    options = args.extract_options!
    options[:builder] = StandardFormBuilder
    options[:html] ||= {}
    options[:html][:class] = "form form-horizontal #{options[:html][:class]}"


    form_for(name, *(args << options), &block)
  end





  class StandardFilterFormBuilder < ActionView::Helpers::FormBuilder
    alias_method :simple_text_field, :text_field
    alias_method :simple_fields_for, :fields_for
    alias_method :simple_check_box, :check_box

    def field_wrapper(field, options={})
      custom_wrapper_class = "form-group"
      custom_wrapper_class += @object.errors[field].empty? ? '' : ' has-error'
      custom_wrapper_class += " #{options[:wrapper_class]}"


      @template.content_tag(:div, :class => custom_wrapper_class) do
        if options[:label] != false
          label(field, options[:label], :class => "col-sm-4 control-label")
        else
          " ".html_safe
        end +
            yield +
            if !@object.errors[field].empty?
              @template.content_tag(:p, :class => "help-block field-errors") do
                @object.errors.full_messages_for(field).map do |msg|
                  @template.content_tag :span, msg
                end.join.html_safe
              end
            end
      end
    end

    def hint(text)
      if text.nil? || text.empty?
        ""
      else
        @template.content_tag(:p, text, :class => 'help-block')
      end
    end

    def first_col_offset(options)
      options[:label] == false ? "col-sm-offset-4" : ""
    end

    def field(field, options={})
      field_wrapper(field, options) do
        @template.content_tag(:div, :class => "col-sm-8 #{first_col_offset(options)}") do
          yield + hint(options[:hint])
        end
      end
    end


    def self.create_tagged_field(method_name)
      define_method(method_name) do |field, *args|
        options = args.extract_options!
        options[:class] = "form-control #{options[:class]}"
        options[:wrapper_class] = "standard-#{method_name.to_s.dasherize} #{options[:wrapper_class]}"


        field(field, options) do
          super(field, *(args + [options.merge(wrapper_class: nil)]))
        end

      end
    end

    [:text_field, :password_field, :text_area].each do |name|
      create_tagged_field(name)
    end

    def datetime_text_fields(field, *args)
      options = args.extract_options!
      options[:class] = "form-control #{options[:class]}"
      options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      date_options = options.clone
      date_options[:class] = "date-subfield #{date_options[:class]}"
      date_options[:wrapper_class] = nil

      time_options = options.clone
      time_options[:class] = "time-subfield #{time_options[:class]}"
      time_options[:wrapper_class] = nil


      field_wrapper(field, options) do
        @template.content_tag(:div, :class => "col-sm-3 #{first_col_offset(options)}") do
          simple_text_field("#{field}_date", date_options)
        end +
            @template.content_tag(:div, :class => "col-sm-2") do
              simple_text_field("#{field}_time", time_options)
            end
      end
    end

    def date_text_field(field, *args)
      options = args.extract_options!
      options[:class] = "date-text-input #{options[:class]}"

      text_field(field, *(args+[options]))
    end

    def select(field, choices, options = {}, html_options = {})
      html_options = options.clone
      html_options[:class] = "form-control #{options[:class]}"
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, choices, options, html_options.merge(wrapper_class: nil))
      end
    end


    def collection_select(field, collection, value_method, text_method, options = {}, html_options = {})
      html_options = options.clone
      html_options[:class] = "form-control #{options[:class]}"
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, collection, value_method, text_method, options, html_options.merge(wrapper_class: nil))
      end
    end

    def grouped_collection_select(field, collection, group_method, group_label_method, option_key_method, option_value_method, options = {}, html_options = {})
      html_options = options.clone
      html_options[:class] = "form-control #{options[:class]}"
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, collection, group_method, group_label_method, option_key_method, option_value_method, options, html_options.merge(wrapper_class: nil))
      end
    end

    def time_zone_select(field, priority_zones = nil, options = {}, html_options = {})
      html_options = options.clone
      html_options[:class] = "form-control #{options[:class]}"
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, priority_zones, options, html_options.merge(wrapper_class: nil))
      end
    end

    def check_box(field, options = {}, checked_value = "1", unchecked_value = "0")
      custom_label = options[:label] || @object.class.human_attribute_name(field)

      @template.content_tag :div, :class => "checkbox" do
        @template.content_tag :label do
          super(field, options, checked_value, unchecked_value) + " #{custom_label}"
        end
      end
    end

    def check_box_field(field, options = {}, checked_value = "1", unchecked_value = "0")
      field(field, options.merge(label: false)) do
        check_box(field, options, checked_value, unchecked_value)
      end
    end

    def collection_check_boxes(field, collection, value_method, text_method, options = {}, html_options = {}, &block)
      html_options = options.clone
      html_options[:wrapper_class] = "standard-#{__method__.to_s.dasherize} #{options[:wrapper_class]}"

      field(field, html_options) do
        super(field, collection, value_method, text_method, options, html_options.merge(wrapper_class: nil)) do |b|
          @template.content_tag :div, :class => "checkbox" do
            @template.content_tag :label do
              b.check_box + b.text
            end
          end
        end
      end
    end

    def buttons
      @template.content_tag :div, :class => 'form-group form-buttons' do
        @template.content_tag :div, :class => 'col-sm-8 col-sm-offset-4' do
          yield
        end
      end
    end

    def submit(label, options={})
      super(label, options.merge(class: 'btn btn-primary', :name => nil))
    end

    def view_all_button(label, options={})
      @template.content_tag :a, label, class: 'btn btn-default', :href => @template.url_for(@template.params.merge(:f => nil, :utf8 => nil))
    end

  end









  def standard_filter_form_for(name, *args, &block)
    options = args.extract_options!
    options[:builder] = StandardFilterFormBuilder
    options[:method] ||= :get
    options[:url] ||= ""
    options[:html] ||= {}
    options[:html][:class] = "form form-horizontal form-filter #{options[:html][:class]}"


    form_for(name, *(args << options), &block)
  end

  def form_buttons
    content_tag :div, :class => 'form-group form-buttons' do
      content_tag :div, :class => 'col-sm-9 col-sm-offset-3' do
        yield
      end
    end
  end

  def filter_form_buttons
    content_tag :div, :class => 'form-group form-buttons' do
      content_tag :div, :class => 'col-sm-8 col-sm-offset-4' do
        yield
      end
    end
  end




end