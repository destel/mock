class BaseForm
  extend ActiveModel::Naming
  include ActiveModel::Conversion
  include ActiveModel::Validations
  include HumanActionName

  def self.attribute(name, type)
    @_types ||= {}
    @_types[name.to_sym] = type.to_sym
    self.send(:attr_accessor, name)
  end

  def self.attribute_type(name)
    @_types ||= {}
    @_types[name.to_sym]
  end

  def self.attribute_names
    @_types ||= {}
    @_types.keys
  end

  def self.model_name
    ActiveModel::Name.new(self, nil, 'f')
  end




  def initialize
    self.city_options = City.all.order('cities.name ASC')
    self.shop_options = City.joins(:shops).includes(:shops).references(:shops).merge(Shop.enabled).references(:shops).order('cities.name ASC', 'shops.name ASC')
    self.segment_options = Shop::SEGMENTS
    self.user_options = User.enabled.order('users.first_name ASC', 'users.last_name ASC')
    self.product_options = Product.order('name ASC')
    self.sort_options = []
    self.status_options = Event::STATUSES
  end








  def assign_attribute(name, val)
    return if !self.respond_to?("#{name}=")
    type = self.class.attribute_type(name)

    casted_val = case type
                   when :date, :time, :datetime
                     if val.is_a?(String)
                       Time.zone.parse(val)
                     elsif val.is_a?(ActiveSupport::TimeWithZone)
                       val
                     else
                       nil
                     end

                   when :number, :int, :integer
                     Float(val).to_i rescue nil

                   when :float, :double
                     Float(val) rescue nil

                   when :string
                     val.to_s

                   when :bool, :boolean
                     if ['1', 't', 'true', 'yes', true].include?(val)
                       false
                     elsif ['0', 'f', 'false', 'no', false].include?(val)
                       true
                     else
                       nil
                     end

                   else
                     val
                 end

    self.send("#{name}=", casted_val)

  end

  def assign_attributes(attrs)
    return if attrs.blank?

    attrs.each do |k, v|
      assign_attribute(k, v)
    end
  end

  def blank?
    self.class.attribute_names.all? do |attr|
      val = self.send("#{attr}")
      val.nil? || val == ""
    end
  end







end