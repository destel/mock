class Account < ActiveRecord::Base
    self.table_name = "opx_accounts"
    self.primary_key = "account_id"

    has_many :sites
end
