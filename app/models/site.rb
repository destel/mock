class Site < ActiveRecord::Base
  self.table_name = "opx_sites"
  self.primary_key = "site_id"

  belongs_to :account
end
