class Endpoint
  attr_accessor :name, :url

  def initialize(name, url)
    @name = name
    @url = url
  end

  def name
    @name.blank? ? @url : @name
  end
end