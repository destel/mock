class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :init_endpoints
  before_filter :init_mock_sites_and_accounts


  def init_endpoints
    @endpoints = [
        Endpoint.new(nil, "http://localhost:8080"),
        Endpoint.new(nil, "http://opx-dev.opera.com:8080"),
    ]
  end

  def init_mock_sites_and_accounts
    site_ids = [314701124666, 314712545545, 314773640516, 314787597199]
    @sites = Site.where(:site_id => site_ids).includes(:account).all
    @accounts = @sites.map{|s| s.account}.to_a.uniq
  end


end
