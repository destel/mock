
$( document ).ready(function() {
    initMockForms();
})


function initMockForms() {
    var mockForms = $(".mock-form");
    var endpointSelect = $(".endpoint-form select")


    mockForms.each(function(i, form) {
       $(form).data("base-url", $(form).attr("action"));
       $(form).find("[name=utf8]").remove()
    })


    endpointSelect.on("change", function(event) {
        mockForms.each(function(i, form) {
            form.action = endpointSelect.val() + $(form).data("base-url")
        })
    });



    mockForms.find("input, select, textarea").on("change", function(event) {
        if (event.target.name == "AuthString")
            return;

        calcAuthString(event.target.form);
    });

    mockForms.on("submit", function(event) {
        saveForm(event.target);
    });

    mockForms.each(function(i, form) {
        restoreForm(form)
    });

    endpointSelect.trigger("change");
}

function asValue(value) {
    if (!value || value == "") {
        return "unspecified";
    } else {
        return value.toString().toLowerCase();
    }
}

function calcAuthString(form) {
    if ($(form).find("[name=AuthString]").length == 0) {
        return;
    }

    var data = $(form).serializeArray().map(function(item) {
        return {
            name: item.name.toLowerCase(),
            value: asValue(item.value)
        }
    }).sort(function(a, b) {
        return a.name > b.name
    })

    var m = ""
    $.each(data, function(i, item) {
        if (item.name != "authstring") {
            m += item.name
            m += item.value
        }
    });

    var accountId = $(form).find("[name=AccountID]").val()
    var apiKey = apiKeys[accountId]
    var hash = CryptoJS.HmacSHA1(m, CryptoJS.enc.Hex.parse(apiKey)).toString(CryptoJS.enc.Hex);

//    console.log(m)
//    console.log(apiKey)
//    console.log(hash)


    $(form).find("[name=AuthString]").val(hash)
}


function savedFormKey(form) {
    return "mock-form-"+$(form).data("name");
}

function saveForm(form) {
    var data = $(form).serializeArray();
    localStorage.setItem(savedFormKey(form), JSON.stringify(data))
}

function restoreForm(form) {
    var strData = localStorage.getItem(savedFormKey(form))
    if (!strData)
        return;

    var data = JSON.parse(strData);

    $.each(data, function(i, item) {
        if (item.name != "AuthString" && item.name != "CurrentTime") {
            var field = $(form).find("[name="+item.name+"]");
            field.val(item.value);
            field.trigger("change")
        }
    });

}